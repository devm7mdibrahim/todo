package com.devM7mdibrahim.todo.features.model

data class TodoItem(
    val id: Int? = null,
    val title: String?,
    val body: String?
)