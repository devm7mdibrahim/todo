package com.devM7mdibrahim.todo.features.ui

import androidx.appcompat.app.AppCompatActivity
import com.devM7mdibrahim.todo.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)