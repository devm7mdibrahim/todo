package com.devM7mdibrahim.todo.domain.model

data class TodoModel(
    var id: Int? =null,
    var title: String?,
    var body: String?
)