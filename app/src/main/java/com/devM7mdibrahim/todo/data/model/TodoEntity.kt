package com.devM7mdibrahim.todo.data.model

data class TodoEntity(
    var id: Int? = null,
    var title: String?,
    var body: String?
)