package com.devM7mdibrahim.todo.data.cache.mapper

interface CacheMapper<CACHED, ENTITY> {
    fun mapFromCached(cached: CACHED): ENTITY
    fun mapToCached(entity: ENTITY): CACHED
}
